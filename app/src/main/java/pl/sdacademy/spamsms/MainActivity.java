package pl.sdacademy.spamsms;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    final int MY_PERMISSIONS_REQUEST_WRITE_SMS = 1;
    final static String TAG = "kru";

    EditText trescSMS;
    EditText phone1;
    EditText phone2;
    EditText phone3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        trescSMS = (EditText) findViewById(R.id.editTextSMS);
        phone1 = (EditText) findViewById(R.id.phone1);
        phone2 = (EditText) findViewById(R.id.phone2);
        phone3 = (EditText) findViewById(R.id.phone3);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSMSClick();

                Snackbar.make(view, "pojszlo :D", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // wlasna funkcja ktora przyjmuje numer telefonu i tresc wiadomosci
    private void sendSms(String phoneNo, String msg) {
        try {
            //wykorzystujemy smsmanager czyli wbudowane api do zarzadzania smsami
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);

            // za pomoca Log mozemy zrobic log momencie wyslania SMS . Ten log jest widoczny w ANdroid Monitorze.
            Log.d(TAG, "SMS Wysłany");

            // wyslanie snackabara
//            Snackbar mySnackbar = Snackbar.make(layoutSms, "Sms Wysłany", Snackbar.LENGTH_SHORT);
//            mySnackbar.show();
//            NaszeMetody.PrintLog("SMS WYSLANY");
//            NaszeMetody.ShowMessage("NO WITAM");
//            NaszeMetody.ShowSnackBar(layoutSms, "SMS WYSLANY");

            // ponizej na dwa sposoby czyscimy wpisane kontrolki
//            numerTelefonu.setText("");
//            tekstWiadomosci.getText().clear();
        } catch (Exception ex) {
            Log.d(TAG, "SMS Nie wysłany");
            ex.printStackTrace();
        }
    }

    public void onSMSClick() {

        // sprawdzamy czy jest przyznay dostep
        if (android.support.v4.app.ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {

            // za pomoca tej funkcji sprawdzamy czy uzytkownik po raz pierwszy juz blokowal dostep do sms
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.SEND_SMS)) {
                // jesli dostep blokowal pokazujemy po co nam to potrzebne
                showExplanation("Potrzebujemy pozwolenia", "Chcemy wysłać SMS który napisałeś, więc potrzebujemy pozwolenia",
                        Manifest.permission.SEND_SMS, MY_PERMISSIONS_REQUEST_WRITE_SMS);
            } else {
                // pokazujemy okienko z prosba za pierwszym razem odrazu systemowe
                requestPermissions(Manifest.permission.SEND_SMS, MY_PERMISSIONS_REQUEST_WRITE_SMS);
            }
        } else {
            // z Edittext za pomoca funkcji getText().toString() wykonanej na kontrolce pobieramy wpisany tekst
            // sendsms to wlasna funkcja do wysylania smsow opisana ponizej
            sendSms(phone1.getText().toString(), trescSMS.getText().toString());
            sendSms(phone2.getText().toString(), trescSMS.getText().toString());
            sendSms(phone2.getText().toString(), trescSMS.getText().toString());
        }
    }

    // funkcja ktora pokazuje okienko systemowe z prosba o dany kod
    private void requestPermissions(String permissionName, int permissionRequestCode) {
        ActivityCompat.requestPermissions(this, new String[]{permissionName}, permissionRequestCode);
    }

    // wlasna funkcja ktora pokazuje okienko z wyjasnieniem prosby o dostep
    private void showExplanation(String title, String message, final String permission, final int permissionRequestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                requestPermissions(permission, permissionRequestCode);
            }
        });
        builder.show();
    }


    // metoda wywolywana za kazdym razem gdy uzytkownik podejmie decyzje o dostepie
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_SMS: {
                // jesli uzytkownik dal anuluj to dlugosc listy bedzie pusta
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sendSms(phone1.getText().toString(), trescSMS.getText().toString());
                    sendSms(phone2.getText().toString(), trescSMS.getText().toString());
                    sendSms(phone2.getText().toString(), trescSMS.getText().toString());                    // dostep przyznany - mozemy zrobic co chcemy
                    Log.d(TAG, "Dostęp przyznany");
                } else {
                    Log.d(TAG, "Dostęp nie przyznany");
                    //  dostep nie przyznany ! musimy obsluzyc ten problem w aplikacji
                    // ponizej dodatkowo sprawdzamy czy zaznaczyl never ask again
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        // user rejected the permission
                        boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                                Manifest.permission.SEND_SMS);
                        if (!showRationale) {
                            Log.d(TAG, "Uzytkownik zaznaczyl never ask again");
                        }
                    }
                }
                return;
            }

            // za pomoca swticha mozna przejrzec czasmi wiele prosb
        }
    }
}
